module.exports = {
    globals: {
        API_BASE_URL: 'https://api.exchangeratesapi.io/latest',
    },
    verbose: true,
    clearMocks: true,
    setupTestFrameworkScriptFile: '<rootDir>/src/setupTests.js',
    transform: {
        '^.+\\.(js|jsx)$': 'babel-jest',
        '^.+\\.(ts|tsx)$': 'ts-jest',
    },
    moduleNameMapper: {
        '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
            '<rootDir>/__mocks__/fileMocks.js',
        '\\.(css|styl)$': '<rootDir>/__mocks__/styleMocks.js',
    },
    testPathIgnorePatterns: ['/node_modules/'],
    transformIgnorePatterns: ['<rootDir>/node_modules/'],
    snapshotSerializers: ['enzyme-to-json/serializer'],
};
