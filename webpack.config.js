const path = require('path');
const dotenv = require('dotenv').config();
const webpack = require('webpack');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const APP_PATH = path.resolve(__dirname, 'src');
const isDev = process.env.NODE_ENV === 'development';
const isProd = !isDev;

const optimization = () => {
    let config = {
        splitChunks: {
            chunks: 'all',
        },
    };

    if (isProd) {
        config.minimizer = [new TerserPlugin(), new OptimizeCssAssetsPlugin()];
    }

    return config;
};

const filename = ext => (isDev ? `[name].${ext}` : `[name].[hash].${ext}`);

const cssLoaders = extra => {
    const loaders = [
        {
            loader: MiniCssExtractPlugin.loader,
            options: {
                hmr: isDev,
                reloadAll: true,
            },
        },
        'css-modules-typescript-loader',
        {
            loader: 'css-loader',
            options: {
                modules: true,
            },
        },
    ];

    if (extra) {
        loaders.push(extra);
    }

    return loaders;
};

module.exports = {
    devServer: {
        port: 4200,
        hot: isDev,
    },
    entry: ['@babel/polyfill', APP_PATH],
    mode: 'development',
    optimization: optimization(),
    output: {
        filename: filename('js'),
        path: path.resolve(__dirname, 'dist'),
    },
    plugins: [
        new HTMLWebpackPlugin({
            inject: true,
            template: path.join(APP_PATH, 'index.html'),
            minify: {
                collapseWhitespace: isProd,
            },
        }),
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: filename('css'),
        }),
        new ForkTsCheckerWebpackPlugin(),
        new webpack.DefinePlugin({
            'process.env.API_BASE_URL': JSON.stringify(
                process.env.API_BASE_URL
            ),
        }),
    ],
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
        alias: {
            components: path.resolve(__dirname, 'src/components'),
            widgets: path.resolve(__dirname, 'src/widgets'),
        },
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: cssLoaders(),
            },
            {
                test: /\.(png|svg|jpg|gif|ttf|woff2|eof)$/,
                use: ['file-loader'],
            },
            {
                test: /\.styl$/,
                use: cssLoaders('stylus-loader'),
            },
            {
                test: /\.(ts|js)x?$/,
                loader: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env',
                            '@babel/preset-typescript',
                            '@babel/react',
                        ],
                        plugins: [
                            '@babel/plugin-proposal-class-properties',
                            '@babel/proposal-object-rest-spread',
                        ],
                    },
                },
            },
        ],
    },
};
