# React currency converter

Currency converter built with React

Description
A currency converter built with React, Redux, and redux-thunk.

Installation

1. Clone repo: `git clone https://exidniy@bitbucket.org/exidniy/rta.git`
2. `cd rta`
3. Run `npm install`

Usage
Run `npm start`
