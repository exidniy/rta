import { IRates } from '../../actions/conversion/types';

interface IWallet {
    currency: string;
    value: string | number;
    symbol: string;
}

export interface IConversionState {
    fromData: IWallet
    toData: IWallet
    rates: IRates | null;
    isLoading: boolean;
}
