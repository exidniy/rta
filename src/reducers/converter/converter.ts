import * as constants from '../../actions/conversion/constants';
import * as balanceConstants from '../../actions/balance/constants';
import { ConversionActionTypes } from '../../actions/conversion/types';
import { SetBalanceAction } from '../../actions/balance/types';
import { IConversionState } from './types';

const initialState: IConversionState = {
    fromData: {
        currency: 'USD',
        value: '',
        symbol: '$',
    },
    toData: {
        currency: 'EUR',
        value: '',
        symbol: '€',
    },
    rates: null,
    isLoading: false,
};

const converter = (
    state = initialState,
    action: ConversionActionTypes | SetBalanceAction
) => {
    switch (action.type) {
        case constants.CURRENCY_CHANGED:
            const { selection, currency, symbol } = action.payload;

            return {
                ...state,
                [selection]: {
                    ...state[selection],
                    currency,
                    symbol,
                },
            };
        case constants.GET_CONVERSION_RATE_SUCCESS:
            const { rates, amount } = action.payload;

            return {
                ...state,
                toData: {
                    ...state.toData,
                    value: amount,
                },
                rates,
            };
        case constants.FULFILL_CONVERSION_SUCCESS: {
            const { value, amount } = action.payload;

            return {
                ...state,
                fromData: {
                    ...state.fromData,
                    value,
                },
                toData: {
                    ...state.toData,
                    value: amount,
                },
            };
        }
        case balanceConstants.SET_BALANCE:
            return {
                ...state,
                fromData: {
                    ...state.fromData,
                    value: '',
                },
                toData: {
                    ...state.toData,
                    value: '',
                },
            };

        default:
            return state;
    }
};

export default converter;
