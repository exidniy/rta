import { IBalanceState } from './types';
import * as constants from '../../actions/balance/constants';
import { SetBalanceAction } from '../../actions/balance/types';
import { getPreciseNumber } from '../../actions/conversion/conversion';

export const initialState: IBalanceState = {
    USD: { balance: 100 },
    EUR: { balance: 100 },
    GBP: { balance: 100 },
};

const balance = (state = initialState, action: SetBalanceAction) => {
    switch (action.type) {
        case constants.SET_BALANCE:
            const { fromData, toData } = action.payload;

            return {
                ...state,
                [fromData.currency]: {
                    ...state[fromData.currency],
                    balance: getPreciseNumber(state[fromData.currency].balance - Number(fromData.value)),
                },
                [toData.currency]: {
                    ...state[toData.currency],
                    balance: getPreciseNumber(state[toData.currency].balance + Number(toData.value)),
                },
            };
        default:
            return state;
    }
};

export default balance;
