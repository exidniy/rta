export interface IBalanceState {
	[index: string]: { balance: number };
}
