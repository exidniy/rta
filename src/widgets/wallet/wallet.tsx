import React, { FC } from 'react';
import { Input, Select } from '../../components';
import { IWallet } from './models';
import { CURRENCY_OPTIONS } from '../../config';
import { useDispatch } from 'react-redux';
import {
    changeCurrency,
    fullFillConversion,
} from '../../actions/conversion/conversion';
import styles from './styles/styles.styl';

const   Wallet: FC<IWallet> = ({
    autoFocus = false,
    name,
    balance,
    value,
    data,
    prefix,
    readOnly = false,
    rateInfo,
}) => {
    const dispatch = useDispatch();

    const toCurrency = (value: string) => {
        return value
            .replace(/[^0-9.]/g, '')
            .replace(/(\..*)\./g, '$1')
            .replace(/^(0|\.)+/, '');
    };

    const handleCurrencyChange = (value: string) => {
        dispatch(changeCurrency(name, value));
    };

    const handleChange = (value: string) => {
        dispatch(fullFillConversion(toCurrency(value)));
    };

    return (
        <div className={styles.root}>
            <div className={styles.controls}>
                <Select
                    options={CURRENCY_OPTIONS}
                    value={data.currency}
                    handleChange={handleCurrencyChange}
                />
                <Input
                    name={name}
                    value={value}
                    handleChange={handleChange}
                    readOnly={readOnly}
                    prefix={prefix}
                    autoFocus={autoFocus}
                />
            </div>
            <div className={styles.info}>
                <span>
                    You have {data.symbol}
                    {balance}
                </span>
                {rateInfo && <span>{rateInfo}</span>}
            </div>
        </div>
    );
};

export default Wallet;
