export interface IWallet {
    autoFocus?: boolean;
    name: 'fromData' | 'toData';
    balance: number;
    data: {
        currency: string;
        symbol: string;
    }
    value: number | string;
    prefix: string;
    readOnly: boolean;
    rateInfo?: string;
}
