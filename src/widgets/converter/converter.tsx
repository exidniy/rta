import React, { FC, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { IConverterProps } from './models';
import { Layout, Button } from '../../components';
import styles from './styles/styles.styl';
import { Wallet } from '../';
import { RootState } from '../../store';
import {
    getConversionRate,
    getPreciseNumber,
} from '../../actions/conversion/conversion';
import { setBalance } from '../../actions/balance/balance';
import cn from 'classnames';

const selectConverter = (state: RootState) => state.converter;
const selectWallets = (state: RootState) => state.wallets;

const Converter: FC<IConverterProps> = ({ className }) => {
    const { fromData, toData, rates } = useSelector(selectConverter);
    const wallets = useSelector(selectWallets);
    const dispatch = useDispatch();

    useEffect(() => {
        // @ts-ignore
        let timerId = setTimeout(function fetchRate() {
            dispatch(getConversionRate());
            timerId = setTimeout(fetchRate, 10000);
        }, 0);

        return () => {
            clearTimeout(timerId);
        }
    }, []);

    const isDisabled =
        !fromData.value ||
        Number(fromData.value) > wallets[fromData.currency].balance ||
        fromData.currency === toData.currency;

    const handleExchange = () => {
        dispatch(setBalance(fromData, toData));
    };

    const rateInfo = rates
        ? `${fromData.symbol}1\u00A0=\u00A0${toData.symbol}${getPreciseNumber(
              rates[toData.currency]
          )}`
        : undefined;

    return (
        <div className={cn(styles.root, className)}>
            <Layout className={styles.layout}>
                <div>
                    <Wallet
                        name="fromData"
                        data={fromData}
                        balance={wallets[fromData.currency].balance}
                        value={fromData.value}
                        prefix="‒"
                        readOnly={fromData.currency === toData.currency}
                        autoFocus
                    />
                    <Button
                        className={styles.button}
                        disabled={isDisabled}
                        handleClick={handleExchange}
                        text="Exchange"
                    />
                </div>
                <div>
                    <Wallet
                        name="toData"
                        data={toData}
                        balance={wallets[toData.currency].balance}
                        value={toData.value}
                        prefix="+"
                        readOnly
                        rateInfo={rateInfo}
                    />
                </div>
            </Layout>
        </div>
    );
};

export default Converter;
