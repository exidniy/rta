export const CURRENCY_OPTIONS = [
    {
        name: 'EUR',
        value: 'EUR',
        symbol: '€',
    },
    {
        name: 'GBP',
        value: 'GBP',
        symbol: '£',
    },
    {
        name: 'USD',
        value: 'USD',
        symbol: '$',
    },
];
