import React, { Component } from 'react';
import Converter from './widgets/converter/converter';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { rootReducer } from './store';
import thunk from 'redux-thunk';
import styles from './styles/styles.styl';

const store = createStore(rootReducer, applyMiddleware(thunk));
class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Converter className={styles.root} />
            </Provider>
        );
    }
}

export default App;
