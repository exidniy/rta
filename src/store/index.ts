import { combineReducers } from 'redux'

import converterReducer from '../reducers/converter/converter';
import balanceReducer from '../reducers/balance/balance';

export const rootReducer = combineReducers({
    converter: converterReducer,
    wallets: balanceReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
