import React, { FC } from 'react';
import { ISelectProps } from './models';
import UISelect from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {
    createMuiTheme,
    makeStyles,
    ThemeProvider,
} from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        type: 'dark',
    },
});

const useStyles = makeStyles(theme => ({
    root: {
        color: theme.palette.type,
    },
}));

const Select: FC<ISelectProps> = ({ options, value, handleChange }) => {
    const classes = useStyles();

    return (
        <UISelect
            id="demo-simple-select-filled"
            value={value}
            onChange={event => handleChange(event.target.value as string)}
            classes={{
                root: classes.root,
            }}
        >
            {options.map((currency, index) => (
                <MenuItem key={index} value={currency.value}>
                    {currency.name}
                </MenuItem>
            ))}
        </UISelect>
    );
};

export default function CustomStyles(props: ISelectProps) {
    return (
        <ThemeProvider theme={theme}>
            <Select {...props} />
        </ThemeProvider>
    );
}
