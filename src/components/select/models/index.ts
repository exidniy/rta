interface IOption {
    name: string;
    value: string;
}

export interface ISelectProps {
    options: IOption[];
    value: string;
    handleChange: (value: string) => void;
}
