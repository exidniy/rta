import React, { FC } from 'react';
import { IButtonProps } from './models';
import styles from './styles/styles.styl';
import cn from 'classnames';

const Button: FC<IButtonProps> = ({
    className,
    disabled,
    handleClick,
    text,
}) => {
    return (
        <button
            className={cn(styles.root, className)}
            disabled={disabled}
            onClick={handleClick}
        >
            {text}
        </button>
    );
};

export default Button;
