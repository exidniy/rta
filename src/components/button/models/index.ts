export interface IButtonProps {
    className: string;
    handleClick: () => void;
    disabled: boolean;
    text: string;
}
