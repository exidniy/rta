import React, { FC } from 'react';
import styles from './styles/styles.styl';
import { ILayout } from './models';
import cn from 'classnames';

const Layout: FC<ILayout> = props => {
    const { className } = props;

    return <div className={cn(styles.root, className)}>{props.children}</div>;
};

export default Layout;
