export interface IInputProps {
    autoFocus: boolean;
    handleChange: (value: string) => void;
    name: string;
    prefix: string;
    readOnly: boolean;
    value: number | string;
}
