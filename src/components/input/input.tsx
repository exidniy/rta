import React, { FC } from 'react';
import { IInputProps } from './models';
import styles from './styles/styles.styl';

const setPrefix = (value: number | string, prefix: string) => prefix + value;
const removePrefix = (value: string, prefix: string) =>
    value.replace(prefix, '');

const Input: FC<IInputProps> = ({
    autoFocus,
    handleChange,
    readOnly,
    prefix,
    value,
}) => {
    return (
        <input
            autoFocus={autoFocus}
            className={styles.root}
            type="text"
            value={prefix && value ? setPrefix(value, prefix) : value}
            onChange={event =>
                prefix
                    ? handleChange(removePrefix(event.target.value, prefix))
                    : handleChange(event.target.value)
            }
            readOnly={readOnly}
        />
    );
};

export default Input;
