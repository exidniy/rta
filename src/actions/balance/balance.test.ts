import * as actions from './balance';
import * as types from './constants';

describe('balance actions', () => {
    it('should create an action to set a new balance', () => {
        const payload = {
        	fromData: {
        		currency: 'USD',
		        value: 100
	        },
	        toData: {
        		currency: 'EUR',
		        value: 91
	        }
        };

        const expectedAction = {
            type: types.SET_BALANCE,
	        payload,
        };
        expect(actions.setBalance(payload.fromData, payload.toData)).toEqual(expectedAction);
    });
});
