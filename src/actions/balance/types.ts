import { SET_BALANCE } from './constants';

export interface SetBalanceAction {
	type: typeof SET_BALANCE
	payload: {
		[key: string]: {
			currency: string;
			value: number | string;
		}
	};
}
