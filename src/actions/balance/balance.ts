import * as constants from './constants';
import { SetBalanceAction } from './types';

const setBalance = <T extends { currency: string; value: number | string }>(
    fromData: T,
    toData: T
): SetBalanceAction => {
    return {
        type: constants.SET_BALANCE,
        payload: {
            fromData,
            toData,
        },
    };
};

export { setBalance };
