import * as constants from './constants';
import { ConversionActionTypes, IRates } from './types';
import axios from 'axios';
import { AppThunk } from '../common.types';
import { CURRENCY_OPTIONS } from '../../config';

const API_BASE_URL = process.env.API_BASE_URL;

export const getPreciseNumber = (value: number | string): number => {
    return Math.floor(Number(value) * 100) / 100;
};

const getConversionRate = (): AppThunk => {
    return (dispatch, getState) => {
        const { currency: fromCurrency } = getState().converter.fromData;
        const { currency: toCurrency } = getState().converter.toData;

        if (fromCurrency === toCurrency) {
            const value = getState().converter.fromData.value;
            dispatch(getConversionRateSuccess(null, value));
            return Promise.resolve();
        }

        return axios
            .get(`${API_BASE_URL}`, {
                params: {
                    base: fromCurrency,
                    t: new Date().getTime()
                },
            })
            .then(response => {
                const value = getState().converter.fromData.value;

                const result = response.data;
                const rates = result.rates;

                let rate = getPreciseNumber(rates[toCurrency]);
                const precisedValue = getPreciseNumber(value);

                dispatch(
                    getConversionRateSuccess(
                        rates,
                        value ? getPreciseNumber(precisedValue * rate) : ''
                    )
                );
            });
    };
};

export const getConversionRateSuccess = (
    rates: IRates | null,
    amount: number | string
): ConversionActionTypes => {
    return {
        type: constants.GET_CONVERSION_RATE_SUCCESS,
        payload: {
            rates,
            amount,
        },
    };
};

const fullFillConversion = (value: string): AppThunk => {
    return (dispatch, getState) => {
        const toCurrency = getState().converter.toData.currency;
        const rates = getState().converter.rates;

        if (!rates) {
            return;
        }

        const rate = rates[toCurrency];

        const precisedValue = getPreciseNumber(value);

        dispatch(
            fulfillConversionSuccessfully(
                value,
                value ? getPreciseNumber(rate * precisedValue) : ''
            )
        );
    };
};

export const fulfillConversionSuccessfully = (
    value: string | number,
    amount: number | string
): ConversionActionTypes => {
    return {
        type: constants.FULFILL_CONVERSION_SUCCESS,
        payload: {
            value,
            amount,
        },
    };
};

const getCurrencySymbol = (currency: string): string => {
    return CURRENCY_OPTIONS.find(item => item.value === currency)!.symbol;
};

const changeCurrency = (
    selection: 'fromData' | 'toData',
    currency: string
): AppThunk => {
    return (dispatch, getState) => {
        const currentCurrency = getState().converter[selection].currency;

        if (currency === currentCurrency) {
            return;
        }

        const symbol = getCurrencySymbol(currency);

        dispatch(changeCurrencySuccess(selection, currency, symbol));
        dispatch(getConversionRate());
    };
};

export const changeCurrencySuccess = (
    selection: 'fromData' | 'toData',
    currency: string,
    symbol: string
): ConversionActionTypes => {
    return {
        type: constants.CURRENCY_CHANGED,
        payload: {
            selection,
            currency,
            symbol,
        },
    };
};

export { fullFillConversion, getConversionRate, changeCurrency };
