import * as constants from './constants';

export interface IRates {
    [key: string]: number;
}

export interface GetConversionRateSuccess {
    type: typeof constants.GET_CONVERSION_RATE_SUCCESS;
    payload: {
        rates: IRates | null
        amount: number | string;
    };
}

export interface FullFillConversionSuccessAction {
    type: typeof constants.FULFILL_CONVERSION_SUCCESS;
    payload: {
        value: string | number;
        amount: number | string;
    };
}

export interface FullFillConversionFailureAction {
    type: typeof constants.FULFILL_CONVERSION_FAILURE;
}

export interface ChangeCurrencySuccessAction {
    type: typeof constants.CURRENCY_CHANGED;
    payload: {
        selection: 'fromData' | 'toData';
        currency: string;
        symbol: string;
    };
}

export type ConversionActionTypes =
    | GetConversionRateSuccess
    | FullFillConversionSuccessAction
    | FullFillConversionFailureAction
    | ChangeCurrencySuccessAction;
