import * as actions from './conversion';
import * as types from './constants';
import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';
const middlewares = [thunk];
const mockStore = configureStore(middlewares);
import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';
const mock = new MockAdapter(axios);

const API_BASE_URL = process.env.API_BASE_URL;

describe('conversion sync actions', () => {
    it('should create an action to set a new conversion rate', () => {
        const payload = {
            rates: null,
            amount: 50,
        };

        const expectedAction = {
            type: types.GET_CONVERSION_RATE_SUCCESS,
            payload,
        };
        expect(
            actions.getConversionRateSuccess(payload.rates, payload.amount)
        ).toEqual(expectedAction);
    });

    it('should create an action to fulfill conversion', () => {
        const payload = {
            value: 100,
            amount: 91,
        };

        const expectedAction = {
            type: types.FULFILL_CONVERSION_SUCCESS,
            payload,
        };
        expect(
            actions.fulfillConversionSuccessfully(payload.value, payload.amount)
        ).toEqual(expectedAction);
    });

    it('should create an action to change currency', () => {
        const payload = {
            selection: 'fromData' as 'fromData',
            currency: 'EUR',
            symbol: '€',
        };

        const expectedAction = {
            type: types.CURRENCY_CHANGED,
            payload,
        };
        expect(
            actions.changeCurrencySuccess(
                payload.selection,
                payload.currency,
                payload.symbol
            )
        ).toEqual(expectedAction);
    });
});

describe('conversion sync actions', () => {
    it('creates GET_CONVERSION_RATE_SUCCESS when currencies in both selects are equal', () => {
        const expectedActions = [
            {
                type: types.GET_CONVERSION_RATE_SUCCESS,
                payload: {
                    rates: null,
                    amount: 100,
                },
            },
        ];

        const mockState = {
            converter: {
                fromData: {
                    currency: 'USD',
                    value: 100,
                },
                toData: {
                    currency: 'USD',
                    value: 100,
                },
                rates: {
                    EUR: 0.91,
                },
                isLoading: false,
            },
        };

        const store = mockStore(mockState);

        // @ts-ignore
        return store.dispatch(actions.getConversionRate()).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

    it('creates GET_CONVERSION_RATE_SUCCESS when fetching rate has been done', () => {
        const expectedActions = [
            {
                type: types.GET_CONVERSION_RATE_SUCCESS,
                payload: {
                    rates: {
                        GBP: 0.64,
                    },
                    amount: 12.8,
                },
            },
        ];

        const mockState = {
            converter: {
                fromData: {
                    currency: 'USD',
                    value: 20,
                },
                toData: {
                    currency: 'GBP',
                    value: '',
                },
                rates: null,
                isLoading: false,
            },
        };

        const store = mockStore(mockState);

        mock.onGet(API_BASE_URL).reply(200, {
            data: {
                rates: {
                    GBP: 0.64,
                },
            },
        });

        // @ts-ignore
        store.dispatch(actions.getConversionRate()).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });
    });
});

describe('getPreciseNumber helper', () => {
    it('should return float number with precision 2', () => {
        expect(actions.getPreciseNumber(1)).toEqual(1);
        expect(actions.getPreciseNumber(100.1)).toEqual(100.1);
        expect(actions.getPreciseNumber(10.12345)).toEqual(10.12);
    });
});
